package ab_main;

import java.net.URL;
import java.util.ResourceBundle;

import aa_config.Config_project;
import javafx.animation.FadeTransition;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class StartController implements Initializable{
	
	 
	    @FXML
	    private ImageView imageStartup;
	    
	     
	    
	    @Override
	    public void initialize(URL url, ResourceBundle rb) {
	        // TODO
	    	
	    	
	    	 
	    	 
	    	FadeTransition fadeTransition=  new FadeTransition();
			fadeTransition.setDuration(Duration.millis(Config_project.START_WINDOW_DURATION_IMAGE));
			
	        fadeTransition.setFromValue(Config_project.START_WINDOW__FROM);
	        fadeTransition.setToValue(Config_project.START_WINDOW_TRANSITION_TO);
		     
            fadeTransition.setOnFinished(new EventHandler<ActionEvent>()
            {

		       
		            @Override
		            public void handle(ActionEvent event) {
		                
		                Stage loginScreen=new Stage();
		                Parent root=null;
		                
		                try {
		                    root=FXMLLoader.load(getClass().getResource("../ac_login/Login.fxml"));
		                } catch (Exception e) {
		                }
		                
		                Stage current=(Stage)imageStartup.getScene().getWindow();
		                Scene scene=new Scene(root,Config_project.LOGIN_WIDTH,Config_project.LOGIN_HEIGHT);
		                
		                loginScreen.setScene(scene);
		        		if(!Config_project.TEST)
		                loginScreen.initStyle(StageStyle.TRANSPARENT);
		                current.hide();
		                loginScreen.show();
		                
		            }
		        }
            );
            
            
            fadeTransition.setNode(imageStartup);   
		    fadeTransition.play();
			
		
	        
		    System.out.println("You clicked me!"+imageStartup.getId());
	       
	        
	    }    
	    
	
}
