package ab_main;
	
import aa_config.Config_project;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.fxml.FXMLLoader;

 
public class Main extends Application {
	@Override
	public void start(Stage primaryStage)  throws Exception {
		 
		Parent root = (Parent)FXMLLoader.load(getClass().getResource("Start.fxml"));
		
		Scene scene = new Scene(root,Config_project.START_WIDTH,Config_project.START_HEIGHT);
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		
		primaryStage.getIcons().add(new Image("zz_Images/aa_icon.png"));
		primaryStage.setTitle(Config_project.COMPANY_NAME);
		
		if(!Config_project.TEST)
	 	primaryStage.initStyle(StageStyle.TRANSPARENT);
		
		primaryStage.setScene(scene);
		primaryStage.show();
		
		
		
		 
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
